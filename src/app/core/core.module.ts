import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageOneComponent } from './page-one/page-one.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [PageOneComponent]
})
export class CoreModule { }
